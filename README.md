# OCR Sudoku Solver 2021
MakeFile:\
    all = solver image interface xor\
    solver = solver\
    image = image\
    interface = interface\
    xor = xor\
    clean = remove all 4\
Usage:\
    solver <path to grid>(format below)\
    image\
    interface\
    xor\
Image format: '.' is a digit, use '.' for a 0, space every 3 digits, '\n' every 9, another '\n' every 3 lines \
... ... ...\
... ... ...\
... ... ...\
\
... ... ...\
... ... ...\
... ... ...\
\
... ... ...\
... ... ...\
... ... ...\
