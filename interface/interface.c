#include <string.h>
#include <gtk/gtk.h>
#include <gtk/gtkx.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "../headers/resize.h"
#include "../headers/rotation.h"
#include "../headers/bitmap.h"
#include "../headers/solver.h"
#include "../headers/inputmanager.h"


//window1
//Base window1 settings
GtkWidget *window1;
GtkWidget *fixed1;
GtkWidget *Kringeteaupya;


//buttons
GtkWidget *enter;
GtkWidget *rmanuelle;
GtkWidget *rautomatique;
GtkWidget *choosefile;

//Scale
GtkWidget *scalemanuelle;

//images
GtkWidget *logo;
GtkWidget *image;
GtkWidget *imagerot;

//builder
GtkBuilder *builder;

//window2
//Base window2 settings
GtkWidget *window2;
GtkWidget *fixed2;

//buttons
GtkWidget *enter2;
GtkWidget *reco;
GtkWidget *solv;

//progress bars
//GtkWidget *prog1;
//GtkWidget *prog2;
//GtkWidget *prog3;

//images
GtkWidget *pingouin1;
GtkWidget *pingouin12;
GtkWidget *pingouin2;
GtkWidget *pingouin22;
GtkWidget *pingouin3;
GtkWidget *pingouin32;

//builder
GtkBuilder *builder2;

//window3
//Base window3 settings
GtkWidget *window3;
GtkWidget *fixed3;

//buttons
GtkWidget *save;
GtkWidget *precedent;

//images
GtkWidget *imageout;

//builer
GtkBuilder *builder3;



gchar *filename;
char *filetmp = "images/tmp.png";
int w = 1;
int rotman = 0;
//char sleep = 0;

void posmain()
{
	//sleep(5);
	//traitement
	gtk_widget_hide(pingouin1);
	pingouin12 = gtk_image_new_from_file("../images/green.png");
	gtk_container_add (GTK_CONTAINER (fixed2),pingouin12);
	gtk_widget_show(pingouin12);
	gtk_fixed_move (GTK_FIXED(fixed2), pingouin12, 400, 150);
	gtk_widget_show(window2);
	
	//sleep(5);
	/*/rec
	gtk_container_remove (GTK_CONTAINER (fixed2), pingouin1);
	gtk_widget_hide(image);
	pingouin12 = gtk_image_new_from_file("../images/green.bmp");
	gtk_container_add (GTK_CONTAINER (fixed2),pingouin12);
	gtk_widget_show(pingouin12);
	gtk_fixed_move (GTK_FIXED(fixed2), pingouin12, 400, 150);
	sleep(5);
	//solve
	gtk_container_remove (GTK_CONTAINER (fixed2), pingouin1);
	gtk_widget_hide(image);
	pingouin12 = gtk_image_new_from_file("../images/green.bmp");
	gtk_container_add (GTK_CONTAINER (fixed2),pingouin12);
	gtk_widget_show(pingouin12);
	gtk_fixed_move (GTK_FIXED(fixed2), pingouin12, 400, 150);*/
}

int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
	//window1 init
	//Base instructions initialisation
	builder = gtk_builder_new_from_file("interface/interface.glade");
	window1 = GTK_WIDGET(gtk_builder_get_object(builder,"window1"));
	g_signal_connect(window1, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	gtk_builder_connect_signals(builder, NULL);
	fixed1 = GTK_WIDGET(gtk_builder_get_object(builder, "fixed1"));
	Kringeteaupya = GTK_WIDGET(gtk_builder_get_object(builder, "Kringeteaupya"));
	
	//buttons initialisation	
	enter = GTK_WIDGET(gtk_builder_get_object(builder, "enter"));	
	rmanuelle = GTK_WIDGET(gtk_builder_get_object(builder, "rmanuelle"));	
	rautomatique = GTK_WIDGET(gtk_builder_get_object(builder, "rautomatique"));
	choosefile = GTK_WIDGET(gtk_builder_get_object(builder, "choosefile"));
	
	//scale initialisation	
	scalemanuelle = GTK_WIDGET(gtk_builder_get_object(builder, "scalemanuelle"));
	gtk_range_set_range(GTK_RANGE(scalemanuelle), 0, 360);
        gtk_range_set_value(GTK_RANGE(scalemanuelle), 0);
	
	//images initialisation	
	logo = GTK_WIDGET(gtk_builder_get_object(builder, "logo"));
	imagerot = GTK_WIDGET(gtk_builder_get_object(builder, "imagerot"));
	image = GTK_WIDGET(gtk_builder_get_object(builder, "image"));
	
	//window2
	//Base instructions initialisation
	builder2 = gtk_builder_new_from_file("interface/interface2.glade");
	window2 = GTK_WIDGET(gtk_builder_get_object(builder2,"window2"));
	g_signal_connect(window2, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	gtk_builder_connect_signals(builder2, NULL);
	fixed2 = GTK_WIDGET(gtk_builder_get_object(builder2, "fixed2"));
	
	//buttons initialisation
	enter2 = GTK_WIDGET(gtk_builder_get_object(builder2, "enter2"));
	//reco = GTK_WIDGET(gtk_builder_get_object(builder2, "reco"));
	//solve = GTK_WIDGET(gtk_builder_get_object(builder2, "solve"));
	
	//progress bars
	//prog1 = GTK_WIDGET(gtk_builder_get_object(builder2, "prog1"));
	//prog2 = GTK_WIDGET(gtk_builder_get_object(builder2, "prog2"));
	//prog3 = GTK_WIDGET(gtk_builder_get_object(builder2, "prog3"));
	
	//images
	pingouin1 = GTK_WIDGET(gtk_builder_get_object(builder2, "pingouin1"));
	pingouin2 = GTK_WIDGET(gtk_builder_get_object(builder2, "pingouin2"));
	pingouin3 = GTK_WIDGET(gtk_builder_get_object(builder2, "pingouin3"));
	
	//window3
	//Base instructions initialisation
	builder3 = gtk_builder_new_from_file("interface/interface3.glade");
	window3 = GTK_WIDGET(gtk_builder_get_object(builder3,"window3"));
	g_signal_connect(window3, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	gtk_builder_connect_signals(builder3, NULL);
	fixed3 = GTK_WIDGET(gtk_builder_get_object(builder3, "fixed3"));
	
	//buttons initialisation
	//close = GTK_WIDGET(gtk_builder_get_object(builder3, "close"));
	save = GTK_WIDGET(gtk_builder_get_object(builder3, "save"));
	precedent = GTK_WIDGET(gtk_builder_get_object(builder3, "precedent"));

	imageout = GTK_WIDGET(gtk_builder_get_object(builder3, "imageout"));
	
	//______________________________________________________________
	//______________________________________________________________
	
	if (w==1)
	{
		gtk_widget_show(window1);	
		imagerot = NULL;
	}
	else
		if (w==2)
		{
			gtk_widget_show(window2);
			pingouin1=NULL;
			pingouin2=NULL;
			pingouin3=NULL;
		}
		else
		{
			gtk_widget_show(window3);
			imageout = NULL;
			
		
		}
	
	
	gtk_main();
	
	return EXIT_SUCCESS;
	
}

//window1
//buttons

void on_enter_clicked (GtkButton * b)
{
	if (imagerot==NULL)
	{
		return;
	}
	w=2;
	
	pingouin12 = gtk_image_new_from_file("images/green.png");
	sleep(2);
	gtk_widget_hide(pingouin1);
	gtk_container_add (GTK_CONTAINER (fixed2),pingouin12);
	gtk_widget_show(pingouin12);
	gtk_fixed_move (GTK_FIXED(fixed2), pingouin12, 80, 350);
	gtk_widget_show(window2);
	gtk_widget_hide(window1);
	SDL_Surface *surface = load_image_surface("images/image_02.bmp");
	SDL_Surface *aled = Grayscale(surface);
	SDL_SaveBMP(aled, "gray.bmp");
	SDL_Surface *ah = toBlackAndWhite(aled);
	SDL_SaveBMP(ah, "bw.bmp");
}

void on_rmanuelle_clicked (GtkButton * b)
{
	if (image)
		gtk_container_remove(GTK_CONTAINER(fixed1), image);
	rotman = gtk_range_get_value(GTK_RANGE(scalemanuelle));
	//add fct rot man
	SDL_Surface *surface = load_image_surface(filename);
	SDL_Surface *s = SDL_RotateImage(surface, rotman);
	SDL_SaveBMP(s,filetmp);
	image = gtk_image_new_from_file(filetmp);
	gtk_container_add (GTK_CONTAINER (fixed1),image);
	gtk_widget_show(image);
	gtk_fixed_move(GTK_FIXED(fixed1), image, 400, 150);
	free (s);
	 
}

void on_rautomatique_clicked (GtkButton * b)
{
	if (imagerot)
		gtk_container_remove(GTK_CONTAINER(fixed1), imagerot);
	imagerot = gtk_image_new_from_file(filename);
	//add fct rot auto
	gtk_container_add (GTK_CONTAINER (fixed1),imagerot);
	gtk_widget_show(imagerot);
	gtk_fixed_move(GTK_FIXED(fixed1), imagerot, 400, 150);
}

void on_choosefile_file_set (GtkFileChooserButton *f)
{
	filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER(f));
	//resize(filename);
	if (imagerot)
		gtk_container_remove (GTK_CONTAINER (fixed1), imagerot);
	gtk_widget_hide(image);
	imagerot = gtk_image_new_from_file(filename);
	gtk_container_add (GTK_CONTAINER (fixed1),imagerot);
	gtk_widget_show(imagerot);
	gtk_fixed_move (GTK_FIXED(fixed1), imagerot, 400, 150);
}

//scale
//void on_scalemanuelle_format_value (GtkScale *s)
//{
//	rotman = gtk_range_get_value(GTK_RANGE(s));
//}

//window2
//buttons

void on_enter2_clicked (GtkButton * b)
{
	printf("?\n");
	int *start;
	int *res;
	w=3;
	gtk_widget_show(window3);
	gtk_widget_hide(window2);
	printf("?\n");
	if (filename=="images/image_01.bmp")
		{
		start = from_file("grids/grid00");
		res = from_file("grids/grid00");
		}
	else
	{
		start = from_file("grids/grid03");
		res = from_file("grids/grid03");
		}
	printf("?\n");
	solver(res,0,0);
	SDL_Surface *surface = SDL_CreateRGBSurface (0,450,450,32,0,0,0,0);
	printf("?\n");
	outimg(surface,start,res);
	printf("?\n");
	SDL_SaveBMP(surface,filetmp);
	SDL_SaveBMP(surface,"solve.bmp");
	imagerot = gtk_image_new_from_file("solve.bmp");

	SDL_SaveBMP(surface,"Solved.bmp");
	imagerot = gtk_image_new_from_file("Solved.bmp");

	printf("??\n");
	gtk_container_add (GTK_CONTAINER (fixed3),imagerot);
	printf("???\n");
	gtk_widget_show(imagerot);
	printf("????\n");
	gtk_fixed_move (GTK_FIXED(fixed3), imagerot, 250, 150);
	printf("?????\n");
	
	
}
void on_reco_clicked (GtkButton * b)
{
	sleep(2);
	pingouin22 = gtk_image_new_from_file("images/green.png");
	if (pingouin22)
		gtk_container_remove (GTK_CONTAINER (fixed2), pingouin22);
	gtk_widget_hide(pingouin2);
	gtk_container_add (GTK_CONTAINER (fixed2),pingouin22);
	gtk_widget_show(pingouin22);
	gtk_fixed_move (GTK_FIXED(fixed2), pingouin22, 380, 350);
	gtk_widget_show(window2);
}
void on_solv_clicked (GtkButton * b)
{
	sleep(4);
	pingouin32 = gtk_image_new_from_file("images/green.png");
	if (pingouin32)
		gtk_container_remove (GTK_CONTAINER (fixed2), pingouin32);
	gtk_widget_hide(pingouin3);
	gtk_container_add (GTK_CONTAINER (fixed2),pingouin32);
	gtk_widget_show(pingouin32);
	gtk_fixed_move (GTK_FIXED(fixed2), pingouin32, 680, 350);
	gtk_widget_show(window2);
}

//window3
//buttons

void on_save_clicked (GtkButton *f)
{
	GtkWidget *dialog;
	GtkFileChooser *chooser;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
	gint res;

	dialog = gtk_file_chooser_dialog_new ("Save File",
                                      GTK_WINDOW(window3),
                                      action,
                                      ("_Cancel"),
                                      GTK_RESPONSE_CANCEL,
                                      ("_Save"),
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
	chooser = GTK_FILE_CHOOSER (dialog);
	gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

  	gtk_file_chooser_set_current_name (chooser,"sudokuResolved");
  	gtk_file_chooser_set_filename (chooser,"sudokuResolved.bmp");

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT)
 	{
 		SDL_Surface *s = load_image_surface(filetmp);
 		SDL_SaveBMP(s,gtk_file_chooser_get_filename (chooser));
    		/*gchar *fileout;
   		fileout = gtk_file_chooser_get_filename (chooser);
    		FILE *file = fopen(fileout, "w+");
    		if(file == NULL)
    		{
        		return;
    		}
    		fprintf(file, "%s", imagerot);
    		fclose(file);
    		g_free (fileout);*/
  	}

	gtk_widget_destroy (dialog);
                             
	
	
}

void on_precedent_clicked (GtkButton * b)
{
	w=1;
	gtk_widget_hide(window3);
	gtk_widget_show(window1);
}

