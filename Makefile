CC = gcc
SOLVER = ./sourcecode/solver_main.c ./sourcecode/solver.c ./sourcecode/inputmanager.c
IMAGE = ./sourcecode/resize.c ./sourcecode/rotation.c ./sourcecode/segmentation.c
NETWORK = ./sourcecode/bitmap.c ./sourcecode/Input.c ./sourcecode/Network.c ./sourcecode/Save.c ./sourcecode/Train.c
INTERFACE =  ./interface/interface.c
INPUT = $(SOLVER) $(IMAGE) $(NETWORK) $(INTERFACE)
FLAGS	 =  -Wno-format -Wno-deprecated-declarations -Wno-format-security
LFLAGS	 = -lm -lSDL2 -ljson-c
CPPFLAGS= `pkg-config --libs sdl` -lSDL_image -MMD `pkg-config --cflags --libs gtk+-3.0` -export-dynamic
OUT = -o eausséaire

all:
	$(CC) $(FLAGS) $(INPUT) $(LFLAGS) $(CPPFLAGS) $(OUT)


clean:
	rm eausséaire
	rm eausséaire.d
