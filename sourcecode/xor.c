#include <stdio.h>
#include <stdlib.h> 
#include <time.h>
#include <math.h>
#include <fcntl.h>

#define NUMPAT 4
#define NUMIN  2
#define NUMHID 2
#define NUMOUT 1
#define rando() ((double)rand()/((double)RAND_MAX+1))

double sigmoid(double x)
{
    return 1 / (1 + exp(-x));
}

double sigmoid_prime(double x)
{
    return x * (1.0 - x);
}

int main() {
    int i, j, k, p, np, aled, ranpat[NUMPAT+1], epoch;
    int NumPattern = NUMPAT;
    int NumInput = NUMIN;
    int NumHidden = NUMHID;
    int NumOutput = NUMOUT;
    double Input[NUMPAT+1][NUMIN+1] = { {0, 0, 0},  {0, 0, 0},  {0, 1, 0},  {0, 0, 1},  {0, 1, 1} };
    double Target[NUMPAT+1][NUMOUT+1] = { {0, 0},  {0, 0},  {0, 1},  {0, 1},  {0, 0} };
    double SumH[NUMPAT+1][NUMHID+1];
    double W_InHid[NUMIN+1][NUMHID+1]; 
    double Hidden[NUMPAT+1][NUMHID+1];
    double SumO[NUMPAT+1][NUMOUT+1];
    double W_HidOut[NUMHID+1][NUMOUT+1];
    double Output[NUMPAT+1][NUMOUT+1];
    double DeltaO[NUMOUT+1];
    double SumDOW[NUMHID+1];
    double DeltaH[NUMHID+1];
    double DeltaW_InHid[NUMIN+1][NUMHID+1]; 
    double DeltaW_HidOut[NUMHID+1][NUMOUT+1];
    double Error, eta = 0.5, alpha = 0.9, smallwt = 0.5;
  
    j = 1;
    while(j <= NumHidden) 
    {   /* initialize W_InHid and DeltaW_InHid */
        for(i = 0; i <= NumInput; i++) 
        { 
            DeltaW_InHid[i][j] = 0.0;
            W_InHid[i][j] = 2.0 * (rando() - 0.5) * smallwt;
        }
        j++;
    }

    k = 1;
    while(k <= NumOutput) 
    {   /* initialize W_HidOut and DeltaW_HidOut */
        for(j = 0; j <= NumHidden; j++) 
        {
            DeltaW_HidOut[j][k] = 0.0;              
            W_HidOut[j][k] = 2.0 * (rando() - 0.5) * smallwt;
        }
        k++;
    }
    

    for(epoch = 0; epoch < 100000; epoch++) 
    {   /* iterate weight updates */
        for(p = 1; p <= NumPattern; p++) 
        {   /* randomize order of training patterns */
            ranpat[p] = p ;
        }

        for(p = 1 ; p <= NumPattern ; p++) 
        {
            np = p + rando() * (NumPattern + 1 - p);
            aled = ranpat[p]; ranpat[p] = ranpat[np]; ranpat[np] = aled;
        }

        Error = 0.0 ;
        for(np = 1; np <= NumPattern; np++) 
        {   /* repeat for all the training patterns */
            p = ranpat[np];
            j = 1;
            while(j <= NumHidden) 
            {   /* compute hidden unit activations */
                SumH[p][j] = W_InHid[0][j] ;
                for(i = 1; i <= NumInput; i++ ) 
                {
                    SumH[p][j] += Input[p][i] * W_InHid[i][j];
                }
                Hidden[p][j] = sigmoid(SumH[p][j]);
                j++;
            }

            k = 1;
            while(k <= NumOutput) 
            {   /* compute output unit activations and errors */
                SumO[p][k] = W_HidOut[0][k];
                for(j = 1; j <= NumHidden; j++) 
                {
                    SumO[p][k] += Hidden[p][j] * W_HidOut[j][k];
                }
                Output[p][k] = sigmoid(SumO[p][k]);
                Error += 0.5 * (Target[p][k] - Output[p][k]) * (Target[p][k] - Output[p][k]);
                DeltaO[k] = (Target[p][k] - Output[p][k]) * sigmoid_prime(Output[p][k]);
                k++;
            }

            j = 1;
            while(j <= NumHidden) 
            {   /* backpropagate errors to hidden layer */
                SumDOW[j] = 0.0 ;
                for(k = 1; k <= NumOutput ; k++) 
                {
                    SumDOW[j] += W_HidOut[j][k] * DeltaO[k];
                }
                DeltaH[j] = SumDOW[j] * Hidden[p][j] * (1.0 - Hidden[p][j]);
                j++;
            }

            j = 1;
            while(j <= NumHidden) 
            {   /* update weights WeightIH */
                DeltaW_InHid[0][j] = eta * DeltaH[j] + alpha * DeltaW_InHid[0][j];
                W_InHid[0][j] += DeltaW_InHid[0][j];
                for(i = 1; i <= NumInput ; i++) 
                { 
                    DeltaW_InHid[i][j] = eta * Input[p][i] * DeltaH[j] + alpha * DeltaW_InHid[i][j];
                    W_InHid[i][j] += DeltaW_InHid[i][j];
                }
                j++;
            }

            k = 1;
            while(k <= NumOutput) 
            {   /* update weights W_HidOut */
                DeltaW_HidOut[0][k] = eta * DeltaO[k] + alpha * DeltaW_HidOut[0][k];
                W_HidOut[0][k] += DeltaW_HidOut[0][k];
                for(j = 1; j <= NumHidden; j++) 
                {
                    DeltaW_HidOut[j][k] = eta * Hidden[p][j] * DeltaO[k] + alpha * DeltaW_HidOut[j][k];
                    W_HidOut[j][k] += DeltaW_HidOut[j][k];
                }
                k++;
            }
        }
        if(epoch%100 == 0) 
        {
            fprintf(stdout, "\nEpoch %-5d :   Error = %f", epoch, Error);
        }

        if(Error < 0.0004) 
        {
            break ;  /* stop learning when 'near enough' */
        }
    }
    
    fprintf(stdout, "\n\nNETWORK DATA - EPOCH %d\n\nPat\t", epoch) ;   /* print network outputs */
    for(i = 1; i <= NumInput ;i++ ) 
    {
        fprintf(stdout, "Input%-4d\t", i) ;
    }

    for(k = 1; k <= NumOutput; k++) 
    {
        fprintf(stdout, "Target%-4d\tOutput%-4d\t", k, k);
    }

    p = 1;
    while(p <= NumPattern) 
    {        
        fprintf(stdout, "\n%d\t", p);
        for( i = 1 ; i <= NumInput ; i++ ) {
            fprintf(stdout, "%f\t", Input[p][i]);
        }
        for( k = 1 ; k <= NumOutput ; k++ ) {
            fprintf(stdout, "%f\t%f\t", Target[p][k], Output[p][k]);
        }
        p++;
    }

    printf("\n");
    return 1 ;
}
