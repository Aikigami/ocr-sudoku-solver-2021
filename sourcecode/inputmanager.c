#include <stdio.h>
#include <stdlib.h>

void to_file(int grid[81],char* path)
{
    FILE *f;
    f = fopen(path,"w");
    for(int i=0;i<9;i++)
    {
        for(int j =0;j<9;j++)
        {
            fputc(grid[i*9+j]+48,f);
            if((j==2) | (j == 5))
            {
                fputc(' ',f);
            }
        }
        fputc('\n',f);
        if((i==2) | (i ==5) )
        {
            fputc('\n',f);
        }
    }
    fclose(f);
    return;
}

int *from_file(char* path)
{
    FILE *f;
    int i=0;;
    f = fopen(path,"r");
    int *grid= (int*)calloc(81,sizeof(int));
    char c;
    c=fgetc(f);
    while(c!= EOF)
    {
        if(c!='\n' && c!=' ')
        {
            if ( c =='.')
            {
                grid[i] = 0;
            }
            else
            {
                grid[i] = c - 48;
            }
            i++;
        }
        c=fgetc(f);
    }
    fclose(f);
    return grid;
}
