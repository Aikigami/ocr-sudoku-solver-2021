#include <SDL2/SDL.h>
#include <stdlib.h>
#include "../headers/bitmap.h"

int *ImgToArray(SDL_Surface *surface)
{
    int *in;
    in = calloc(surface->w * surface->h,sizeof(int));
    Uint32 pixel;
    Uint8 r, g , b;
    for(int i = 0; i < surface->w; i++)
    {
        for(int j = 0; j < surface->h; j++)
        {
            pixel = get_pixel(surface,i,j);
            SDL_GetRGB(pixel, surface->format, &r, &g, &b);
            in[j + i * surface->w] = r > 127 ? 1 : 0;
        }
    }
    return in;
}
