#include <json-c/json.h>
#include <stdio.h>
#include <string.h>
#include "../headers/Network.h"
void write_neuron(Neuron *neuron, int index, int index_max, FILE *fp)
{
    int n = neuron->weightlen;

    char *str = calloc(256, sizeof(char));
    strcat(str, "\t\t\t\t{\n\t\t\t\t\t\"weightlen\" : %d,\n\t\t\t\t\t\"bias\"");
    strcat(str, " : %f,\n\t\t\t\t\t\"zvalue\" : %f,\n\t\t\t\t\t\"dactv\" : %f,\n");
    strcat(str, "\t\t\t\t\t\"dbias\" : %f,\n\t\t\t\t\t\"dz\" : %f,");
    strcat(str,"\n\t\t\t\t\t\"act\" : %f,\n\t\t\t\t\t\"weights\" : [\n");

    fprintf(fp, str, n, neuron->weightlen, neuron->bias,
            neuron->zvalue,neuron->dactv,neuron->dbias,neuron->dz,neuron->act);
    free(str);

    for(int i = 0; i < n-1; i++)
        fprintf(fp, "\t\t\t\t\t\t%f,\n", neuron->weights[i]);

    fprintf(fp, "\t\t\t\t\t\t%f\n", neuron->weights[n-1]);
    fprintf(fp, "\t\t\t\t\t]");
    fprintf(fp,"\n\t\t\t\t\t\"dw\" : [\n");
    for(int i = 0; i < n-1; i++)
        fprintf(fp, "\t\t\t\t\t\t%f,\n", neuron->dw[i]);

    fprintf(fp, "\t\t\t\t\t\t%f\n", neuron->dw[n-1]);

    if (index != index_max)
        fprintf(fp, "\t\t\t\t\t]\n\t\t\t\t},\n");
    else
        fprintf(fp, "\t\t\t\t\t]\n\t\t\t\t}\n\t\t\t]\n");
}

void write_layer(Layer *layer, int index, int index_max, FILE *fp)
{
    int n = layer->neuronslen;

    fprintf(fp, "\t\t{\n\t\t\t\"neuronslen\" : %d,\n\t\t\t\"neurons\" : [\n", n);

    for(int i = 0; i < n; i++)
        write_neuron(&layer->neurons[i], i, n-1, fp);

    if (index != index_max)
        fprintf(fp, "\t\t},\n");
    else
        fprintf(fp, "\t\t}\n\t]\n");
}

void write_network(Network *network, char *filename)
{
    int n = network->nbLayers;
    double f = network->fcost;
    FILE *fp = fopen(filename, "w");

    if(!fp)
    {
        fprintf(stderr, "write_network() error: file open failed '%s'.\n",
                filename);
        return;
    }

    fprintf(fp, "{\"network\" : {\n\t\"nbLayers\" : %d,\n\t\"fcost\" : %f,\n\t\"layers\" : [\n", n,f);

    for(int i = 0; i < n; i++)
        write_layer(&network->layers[i], i, n-1, fp);

    fprintf(fp, "}}");
    fclose(fp);
}

struct json_object *parse_network(char *filename)
{
    char *buffer = "";
    unsigned long length;
    FILE *fp = fopen(filename, "r");

    if(fp)
    {
        fseek(fp, 0, SEEK_END);
        length = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        buffer = malloc(length + 1);
        if(buffer)
            if (fread(buffer, 1, length, fp) != length)
                printf("error parsing file!");
        fclose(fp);
    }

    if(buffer)
    {
        struct json_object *new_obj;
        new_obj = json_tokener_parse(buffer);
        struct json_object *network;
        json_object_object_get_ex(new_obj, "network", &network);
        return network;
    }

    printf("Couldn't read %s", filename);
    return NULL;
}

void parse_neuron_from_file(struct json_object *neuron_object, Neuron *neuron)
{
    struct json_object *weights;
    struct json_object *weightlen;
    struct json_object *bias;
    struct json_object *zvalue;
    struct json_object *dactv;
    struct json_object *dbias;
    struct json_object *act;
    struct json_object *dz;
    struct json_object *dw;

    json_object_object_get_ex(neuron_object, "weights", &weights);
    json_object_object_get_ex(neuron_object, "weightlen", &weightlen);
    json_object_object_get_ex(neuron_object, "bias", &bias);
    json_object_object_get_ex(neuron_object, "zvalue", &zvalue);
    json_object_object_get_ex(neuron_object, "dactv", &dactv);
    json_object_object_get_ex(neuron_object, "dbias", &dbias);
    json_object_object_get_ex(neuron_object, "act", &act);
    json_object_object_get_ex(neuron_object, "dz", &dz);
    json_object_object_get_ex(neuron_object, "dw", &dw);

    int len = json_object_get_int(weightlen);

    neuron->weightlen = json_object_get_int(weightlen);
    neuron->bias = json_object_get_double(bias);
    neuron->zvalue = json_object_get_double(zvalue);
    neuron->dactv = json_object_get_double(dactv);
    neuron->dbias = json_object_get_double(dbias);
    neuron->act = json_object_get_double(act);
    neuron->dz = json_object_get_double(dz);


    if (len > 1)
    {
        neuron->weights = calloc(len, sizeof(double));
        neuron->dw = calloc(len, sizeof(double));
    }
        
    else
    {
        neuron->weights = malloc(sizeof(double));
        neuron->dw = malloc(sizeof(double));
    }
    for(int i = 0; i < len; i++)
        neuron->weights[i] = json_object_get_double(json_object_array_get_idx(weights, i));
    for(int i = 0; i < len; i++)
        neuron->dw[i] = json_object_get_double(json_object_array_get_idx(dw, i));
    
}

void parse_layer_from_file(struct json_object *layer_object, Layer *layer)
{
    struct json_object *neuronslen;
    struct json_object *neurons;
    json_object_object_get_ex(layer_object, "neuronslen", &neuronslen);
    layer->neuronslen = json_object_get_int(neuronslen);

    layer->neurons = calloc(layer->neuronslen, sizeof(Neuron));

    json_object_object_get_ex(layer_object, "neurons", &neurons);

    for(int i = 0; i < json_object_get_int(neuronslen); i++)
        parse_neuron_from_file(json_object_array_get_idx(neurons, i),
                               &layer->neurons[i]);

}

void parse_network_from_file(char *filename, Network *network)
{
    struct json_object *network_object = parse_network(filename);
    struct json_object *layers;
    struct json_object *nbLayers;
    struct json_object *fcost;
    struct json_object *cost;

    json_object_object_get_ex(network_object, "nbLayers", &nbLayers);
    json_object_object_get_ex(network_object, "layers", &layers);
    json_object_object_get_ex(network_object, "fcost", &layers);
    json_object_object_get_ex(network_object, "cost", &layers);

    network->nbLayers = json_object_get_int(nbLayers);

    network->layers = calloc(network->nbLayers, sizeof(Layer));

    int nbLayers_val = json_object_get_int(nbLayers);

    for(int i = 0; i < nbLayers_val; i++)
    {
        struct json_object *layer_val;
        layer_val = json_object_array_get_idx(layers, i);
        parse_layer_from_file(layer_val, &network->layers[i]);
    }
    network->cost = calloc(network->nbLayers,sizeof(double));
    for(int i = 0; i < nbLayers_val; i++)
        network->cost[i] = json_object_get_double(json_object_array_get_idx(cost, i));
    
}

