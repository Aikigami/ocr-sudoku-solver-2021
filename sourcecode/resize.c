#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

void resize(char *filename)
{
	FILE *f;
	int j, h = 1, v = 1, hor = 400, ver = 150;
	char cmd[2048];
	sprintf(cmd, "identify -format %%wx%%h \"%s\"\n", filename);
	f = popen(cmd, "r");
	strcpy(cmd,"");
	fgets(cmd, 512, f);
	fclose (f);
	 
	if (strlen(cmd))
	{
		for (j=0; j<strlen(cmd) && cmd[j]!='x';j++)
		if (cmd[j] == 'x')
		{
			cmd[j]=0;
			sscanf(cmd,"%d",&h);
			sscanf(&cmd[j+1], "%d", &v);
		}
	}
	if (h<100 || v<100)
	{
		return;
	}
	
	int size = 350;
	sprintf(cmd, "convert \"%s\" -resize %dx%d tmp.jpg", filename, size, size);
	system(cmd);
	strcpy(filename,"tmp.jpg");
	
	
	sprintf(cmd, "identify -format %%wx%%h \"%s\"\n", filename);
	f = popen(cmd, "r");
	strcpy(cmd,"");
	fgets(cmd, 512, f);
	fclose (f);
	 
	if (strlen(cmd))
	{
		for (j=0; j<strlen(cmd) && cmd[j]!='x';j++)
		if (cmd[j] == 'x')
		{
			cmd[j]=0;
			sscanf(cmd,"%d",&h);
			sscanf(&cmd[j+1], "%d", &v);
		}
	}
}
