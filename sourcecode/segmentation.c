#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "../headers/bitmap.h"
#include "../headers/segmentation.h"


#define RAYON 5


int IsBlackW(SDL_Surface* image_surface, unsigned ptX, unsigned ptY)
{
Uint8 color;
int IsBlack = 0;
unsigned X=ptX;
unsigned Y=ptY;
 unsigned Xtemp = 0, Ytemp=0;
 unsigned i=0,j=0;
 unsigned RayonEff = RAYON;

if (X - RayonEff > 0)
{
X -= RayonEff;
}else
{
RayonEff = 1;
}
if (Y - RayonEff > 0)
{
Y -= RayonEff;
}else
{
RayonEff = 1;
}

 
 while (i < RayonEff && ((X + i) <= (unsigned)image_surface->w)  && !IsBlack)
   {
      Xtemp = X + i;
      while (j < 5 && ((Y + j) <= (unsigned)image_surface->h)  && !IsBlack){

       Ytemp = Y + j;
       SDL_GetRGB(get_pixel(image_surface, Xtemp, Ytemp)
          , image_surface->format, &color, &color, &color);
       if (color != 255)
     {
       IsBlack = 1;
     }
       j++;
     }
     i++;
   }


return IsBlack;

}


int DetectLine(SDL_Surface *image, int x ,int y)
{
	int largeur = 0;
	int Black = IsBlackW(image,x,y);
	while (x < image->w && Black)
	{
		Black = IsBlackW(image,x,y);
		largeur++;
		x++;
	}
	return largeur;
}

int Detectcollum(SDL_Surface *image, int x, int y)
{
	int largeur=0;
	int Black = IsBlackW(image,x,y);
	while(y < image->h && Black)
	{
		Black = IsBlackW(image,x,y);
		largeur++;
		y++;
	}
	return largeur;
}

void Detectiongrid(SDL_Surface *image)
{
	size_t h = image->h;
	size_t w = image->w;
	size_t cosx = 0;
	size_t cosy = 0;
	size_t lenxmax = 0;
	size_t lenymax = 0;
	size_t lensurfacemax = 0;


 	 
	for(size_t i = 0 ; i < w ; i++)
	{
		for(size_t j = 0 ; j < h ; j++)
		{
			if ((get_pixel(image,i,j)==0))
			{
				size_t leny = DetectLine(image,i,j);
				size_t lenx = Detectcollum(image,i,j);
				if(lenx > lenxmax || leny > lenymax)
				{
					if (lenx*leny > lensurfacemax)
					{
						lenxmax = lenx;
						lenymax  = leny;
						lensurfacemax = lenx * leny;
						cosx = i;
						cosy = j;
					}
				}
			}
		}
	}
	printf("%ld,%ld\n",cosx,cosy);
	printf("%ld,%ld\n",lenxmax,lenymax);
	SDL_Surface *final = cut_image(image,cosx,cosy,lenxmax,lenxmax);
	SDL_SaveBMP(final,"out.bmp");
}

int *Is_blank_line(SDL_Surface *image)
{
	int height = image->h;
	int width = image->w;
	int *h = calloc(height,sizeof(int));
	for(int i = 0 ; i < height ; i++)
	{
		for(int j = 0 ; j < width ; j++)
		{
			h[i] += get_pixel(image, i, j) ? 1 : 0;
		}
	}
	return h;
}

int *Is_blank_collum(SDL_Surface *image)
{
	int height = image->h;
        int width = image->w;
        int *h = calloc(width,sizeof(int));
        for(int i = 0 ; i < height ; i++)
        {
                for(int j = 0 ; j < width ; j++)
                {
                        h[j] += get_pixel(image, i, j) ? 1 : 0;
                }
        }
        return h;
}

int *histo_max(int *histo , size_t len)
{
	
	int max = -1;
	size_t len_max = 1;
	int *max_arr = malloc(sizeof(int));
	for(size_t i = 0 ;i < len ; i++)
	{
		if(histo[i] ==max)
		{
			len_max+=1;
			max_arr = realloc(max_arr, (len_max + 1) * sizeof(int));
			max_arr[len_max - 1] = i;
			continue;
		}
		if(histo[i] > max)
		{
			if(len_max != 1)
			{
				len_max = 1;
				max_arr = realloc(max_arr, sizeof(int));
			}
			histo[0] = i;
		}
	}
	return max_arr;
}
SDL_Surface *cut_image(SDL_Surface *image, int x , int y , int w , int h)
{
    size_t rows = h + y;
    size_t cols = w + x;
    SDL_Surface *new_img = SDL_CreateRGBSurface(0,w,h,32,0,0,0,0);
    for (size_t i = x; i < rows; i++)
    {
        for (size_t j = y; j < cols; j++)
        {
            put_pixel(new_img,i-x,j-y,get_pixel(image,i,j));
        }
    }
    return new_img;
}

void Cutgrid(SDL_Surface *image, int x, int y, int w, int h)
{
	size_t rows = 9*(h+y);
	size_t cols = 9*(w+x);
	char p = 0;
	for(size_t i = y ; i < rows ; i+h)
	{
		for(size_t j = x ; j < cols ; j+w)
		{
			p++;
			SDL_Surface *temp = cut_image(image,i,j,w,h);
			char *tempd = p + ".bmp";
			SDL_SaveBMP(temp,tempd);
		}
	}

}
