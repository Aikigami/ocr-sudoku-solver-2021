#include <SDL2/SDL.h>
#include <stdio.h>
#include "../headers/bitmap.h"
int Init_Sdl()
{
    if((SDL_Init(SDL_INIT_VIDEO) == -1))
    {
        printf("Could not initialize SDL: %s.\n", SDL_GetError());
        return 1;
    }

    //atexit(SDL_Quit);

    return 0;
}

SDL_Surface* load_image_surface(char *path)
{
    SDL_Surface *img;

    img = SDL_LoadBMP(path);

    if(img == NULL)
    {
        fprintf(stderr, "Couldn't load %s: %s\n", path, SDL_GetError());
        exit(1);
    }

    return img;
}


void put_pixel(SDL_Surface *surface, unsigned x, unsigned y, Uint32 pixel)
{
    Uint8 *p = pixel_ref(surface, x, y);

    switch(surface->format->BytesPerPixel)
    {
        case 1:
            *p = pixel;
            break;

        case 2:
            *(Uint16 *)p = pixel;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }
            else
            {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;

        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}




Uint8* pixel_ref(SDL_Surface *surf, unsigned x, unsigned y)
{
    int bpp = surf->format->BytesPerPixel;
    return (Uint8*)surf->pixels + y * surf->pitch + x * bpp;
}


Uint32 get_pixel(SDL_Surface *surface, unsigned x, unsigned y)
{
    Uint8 *p = pixel_ref(surface, x, y);

    switch (surface->format->BytesPerPixel)
    {
        case 1:
            return *p;

        case 2:
            return *(Uint16 *)p;

        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;

        case 4:
            return *(Uint32 *)p;
    }

    return 0;
}


SDL_Surface *Grayscale(SDL_Surface *in)
{
    int rows = in -> h;
    int columns = in -> w;
    SDL_Surface *out = SDL_CreateRGBSurface (0,columns,rows,in->format->BitsPerPixel,0,0,0,0);
    Uint8 r, g, b;
    for (int i = 0;i < rows; i++)
    {
        for (int j = 0; j < columns; j++)
        {
		Uint32 data = get_pixel(in, j, i);
		SDL_GetRGB(data, in->format, &r, &g, &b);
		Uint8 average = 0.3*r + 0.59*g + 0.11*b;
		data = SDL_MapRGB(in->format, average, average, average);
		put_pixel(out, j, i, data);
        }
    }
    return out;
}


SDL_Surface *toBlackAndWhite(SDL_Surface *in)
{
	Uint8 r, g, b;
    int rows = in -> h;
    int columns = in -> w;
    SDL_Surface *out = SDL_CreateRGBSurface (0,columns,rows,in->format->BitsPerPixel,0,0,0,0);
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < columns; j++)
        {
        	Uint32 data = get_pixel(in, j, i);
		SDL_GetRGB(data, in->format, &r, &g, &b);
		
            if (r < 127)
                put_pixel(out,j,i,SDL_MapRGB(in->format,0, 0, 0));
            else
                put_pixel(out,j,i,SDL_MapRGB(in->format,255, 255, 255));
        }
    }
    return out;
}

void *outimg(SDL_Surface *surface, int *start, int *res)
{
	SDL_Surface *tmp;
    char buffer[1024];
	for (int i = 0; i<9;i++)
	{
		for (int j = 0; j<9;j++)
		{
			if (*(start+i*9+j) == *(res+i*9+j))
				{
				printf("i=%d,j=%d\n",i,j);
                snprintf(buffer,sizeof(buffer),"numbers/%dN.bmp",res[i*9+j]);
				tmp = load_image_surface(buffer);
				printf("?\n");
				}
			else
				{
				printf("i=%d,j=%d\n",i,j);
                snprintf(buffer,sizeof(buffer),"numbers/%dB.bmp",res[i*9+j]);
				tmp = load_image_surface(buffer);
				printf("?\n");
				}
			for (int k = i*50;k<(i+1)*50;k++)
			{
				for (int l = j*50;l<(j+1)*50;l++)
				{
                    if ((k==i*50)||(k==(i+1)*50-1)||(l==j*50)||(l==(i+1)*50-1))
                        put_pixel(surface,k,l,0);
                    else
					    put_pixel(surface,k,l,get_pixel(tmp,k-i*50,l-j*50));
				}
			}

		}
	}
	printf("a\n");
	
}

