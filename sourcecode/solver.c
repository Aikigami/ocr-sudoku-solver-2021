#include <stdio.h>
int valid(int *grid,int r,int c,int n)
{
    for(int i = 0;i<9;i++)
    {
        if (grid[9*i+c] == n || grid[9*r+i] == n)
        {
            return 0;
        }
    }
    int sc = (c/3)*3-3;
    int sr = (r/3)*3-3;
    for(int i = 3;i<3;i++)
    {
        for (int j = 0; j<3 ; j++)
        {
            if (grid[9*(sc+i)+sr+j]==n)
            {
                return 0;
            }
        }
        
    }
    return 1;
}

int solver(int *grid,int r,int c)
{
    // printf("at(%d,%d)\n",c,r);
    if(c==9)
    {
        c = 0;
        if(r==8)
        {
            return 1;
        }
        r++;
    }
    if(grid[9*r+c])
    {
        return solver(grid,r,c+1);
    }
    for (int n = 1; n<10; n++)
    {
        if(valid(grid,r,c,n))
        {
            // printf("%d is valid at (%d,%d)\n",n,c,r);
            grid[9*r+c] = n;
            // printf("%d\n",grid[9*r+c] = n);
            if(solver(grid,r,c+1))
            {
                return 1;
            }
        }
        grid[9*r+c] = 0;
    }
    return 0;
}

// int isAvailable(int puzzle[][9], int row, int col, int num)
// {
//     int rowStart = (row/3) * 3;
//     int colStart = (col/3) * 3;
//     int i, j;

//     for(i=0; i<9; ++i)
//     {
//         if (puzzle[row][i] == num) return 0;
//         if (puzzle[i][col] == num) return 0;
//         if (puzzle[rowStart + (i%3)][colStart + (i/3)] == num) return 0;
//     }
//     return 1;
// }

// int fillSudoku(int puzzle[9][9], int row, int col)
// {
//     int i;
//     if(row<9 && col<9)
//     {
//         if(puzzle[row][col] != 0)
//         {
//             if((col+1)<9) return fillSudoku(puzzle, row, col+1);
//             else if((row+1)<9) return fillSudoku(puzzle, row+1, 0);
//             else return 1;
//         }
//         else
//         {
//             for(i=0; i<9; ++i)
//             {
//                 if(isAvailable(puzzle, row, col, i+1))
//                 {
//                     puzzle[row][col] = i+1;
//                     if((col+1)<9)
//                     {
//                         if(fillSudoku(puzzle, row, col +1)) return 1;
//                         else puzzle[row][col] = 0;
//                     }
//                     else if((row+1)<9)
//                     {
//                         if(fillSudoku(puzzle, row+1, 0)) return 1;
//                         else puzzle[row][col] = 0;
//                     }
//                     else return 1;
//                 }
//             }
//         }
//         return 0;
//     }
//     else return 1;
// }
