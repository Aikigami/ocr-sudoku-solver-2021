#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../headers/Network.h"
#include "../headers/Input.h"
#include "../headers/Layer.h"
#include "../headers/Neuron.h"
#include "../headers/Save.h"
#include "../headers/Train.h"
// #include <gtk/gtk.h>
#include <math.h>

int main_NN()
{
    srand(time(NULL));
    Network *network = malloc(sizeof(Network));
    int layers[4] = {28*28,50,50,9};
    create_network(network,4,layers);
    // printf("1\n");
    write_network(network,"Before.txt");
    // printf("2\n");
    trainNetwork(network,1,1000);
    // printf("3\n");
    write_network(network,"After.txt");
    // printf("4\n");
    // freeNetwork(network);
}

