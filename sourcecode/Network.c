#include <stdio.h>
#include <stdlib.h> 
#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <string.h>
#include "../headers/Network.h"

#define Width 28
#define Datalen 0
#define Layernbr 3
#define Layer1len Width
#define Layer2len 15
#define Layer3len 9
#define LAST net->layers[net->nbLayers-1]
#define rando() ((double)rand()/((double)RAND_MAX+1))

double sigmoid(double x)
{
    return 1 / (1 + exp(-x));
}

double sigmoid_prime(double x)
{
    return x * (1.0 - x);
}

double ReLu(double x)
{
    return (0 > x) ? 0 : x;
}

double rdmDouble()
{
    double x, y, u, n;
    u = 0.0;
    while(u >= 1.0 || u == 0.0)
    {
        n = (double)rand();
        x = pow(-1.0, n) * ((double) rand()) / ((double) RAND_MAX);
        n = (double)rand();
        y = pow(-1.0, n) * ((double) rand()) / ((double) RAND_MAX);
        u = x * x + y * y;
    }
    return x * sqrt((-2 * log(u)) / u);
}

void startprop(Network *net,int *input)
{
    for (size_t i = 0; i < net->layers[0].neuronslen; i++)
    {
        net->layers[0].neurons[i].act = input[i];
    }
}

void create_network(Network *net,int nbLayer,int *nbNeurons)
{
    Layer *layers = malloc(nbLayer * sizeof(Layer));
    create_layer(&layers[0],nbNeurons[0], 0);
    for(size_t i = 1;i<nbLayer;i++)
    {
        create_layer(&layers[1],nbNeurons[i], nbNeurons[i-1]);
    }
    net->nbLayers = nbLayer;
    double *cost =malloc(nbNeurons[nbLayer-1] * sizeof(double));
    memset(cost,0,net->layers[net->nbLayers-1].neuronslen);
    net->cost = cost;
    net->fcost = 0;
    // for (int i = 1; i < len - 1; i++)
    // {
    //     previousLayer = currentLayer;
    //     currentLayer = inp + i;
    //     previousLayer->NextLayer = currentLayer;
    //     create_layer(currentLayer, nbNeurons, previousLayer);
    // }
    // previousLayer = currentLayer;
    // currentLayer = inp + len - 1;
    // previousLayer->NextLayer = currentLayer;

    // create_layer(currentLayer, outputNbneurons, previousLayer);
    // currentLayer->NextLayer = NULL;
    // net->nbLayers = nbLayer;
    // net->layers = inp;
    // net->input = inp;
    // net->output = currentLayer;
    // for (int i = 0; i < inputNbNeurons; i++)
    // {
    //     net->input->neurons[i].bias = 0;
    // }
}
void randomizeNeurons(Network *network)
{
    for (size_t i = 0; i < network->nbLayers-1; i++)
    {
        for (size_t j = 0; j < network->layers[i].neuronslen; j++)
        {
            for (size_t k = 0; k < network->layers[i].neurons[j].weightlen; k++)
            {
                network->layers[i].neurons[j].weights[k] = rdmDouble();
                network->layers[i].neurons[k].dw[j] = 0;
            }
            if (i>0)  network->layers[i].neurons[j].bias = rdmDouble();
        }
    }
    for (size_t i = 0; i < network->layers[network->nbLayers-1].neuronslen; i++)
    {
        network->layers[network->nbLayers-1].neurons[i].bias = rdmDouble();
    }
}

void create_layer(Layer *layer,int len,int prevlen)
{
    Neuron *neuron = calloc(len,sizeof(Neuron));
    for (size_t i = 0; i < len; i++)
    {
        newNeuron(&neuron[i],prevlen);
    }
    layer->neurons=neuron;
    layer->neuronslen=len;

}

// void SetNeuron(Neuron *neuron, int l_w, double *weight, int bias)
// {
//     for (int i = 0; i < l_w; i ++)
//         neuron->weights[i] = weight[i];
//     neuron->bias = bias;
//}

void newNeuron(Neuron *neuron, int weightlen)
{
    neuron->zvalue = 0;
    neuron->bias = 0;
    neuron->weights = calloc(weightlen, sizeof(double));
    neuron->weightlen = weightlen;
    neuron->dactv = 0;
	neuron->dw = (double*) calloc(weightlen, sizeof(double));
	neuron->dbias = 0;
	neuron->dz = 0;
    neuron->act=0;
}

// void setNeuron(Neuron *neuron,double *weights,int bias)
// {
//      for (int i = 0; i < neuron->weightlen; i ++)
//      {
//         neuron->weights[i] = weights[i];
//      }

//     neuron->bias = bias;
// }

void feedForward(Network *net)
{
    for (size_t i = 1; i < net->nbLayers; i++)
    {
        for(size_t j = 0; j < net->layers[i].neuronslen; j++)
        {
            net->layers[i].neurons[j].zvalue = net->layers[i].neurons[j].bias;

            for(size_t k = 0; k < net->layers[i-1].neuronslen; k++)
            {
                net->layers[i].neurons[j].zvalue += (net->layers[i].neurons[j].weights[k] * net->layers[i-1].neurons[k].act);
            }
            if(i < net->nbLayers-1)
            {
                net->layers[i].neurons[j].act = ReLu(net->layers[i].neurons[j].zvalue); // IF UNACCURATE USE STEP (X == 0)
            }
            else
            {
                net->layers[i].neurons[j].act = sigmoid(net->layers[i].neurons[j].zvalue);
            }
        }
    }
    
}

void Generate_cost(Network *net,int current,double *expected,int count)
{
    float tmpcost=0;
    float tcost=0;

    for(size_t j=0;j<LAST.neuronslen;j++)
    {
        tmpcost = expected[current] - LAST.neurons[j].act;
        net->cost[j] = (tmpcost * tmpcost)/2;
        tcost = tcost + net->cost[j];
    }   

    net->fcost = (net->fcost + tcost)/count;
}


// void propagation_layer(Layer *current)
// {
//     Layer *tmp = current;

//     while (tmp != NULL)
//     {
//         for (int i = 0; i < tmp->neuronslen; i++)
//         {
//             double net = tmp->neurons[i].bias;

//             for (int j = 0; j < tmp->neurons[i].weightlen; j++)
//                 net += tmp->neurons[i].weights[j] * tmp->PreviousLayer->neurons[j].sig;

//             double out = sigmoid(net);

//             tmp->neurons[i].value = net;
//             tmp->neurons[i].sig = out;
//         }

//         tmp = tmp->NextLayer;
//     }
// }

// void sumNeuron(Neuron *neuron, Neuron *prevNeurons, int isOutput)
// {
//     double sum = neuron->bias;

//     for (int i = 0; i < neuron->weightlen; i++)
//         sum += (neuron->weights[i]) * (prevNeurons + i)->sig;

//     double out = sigmoid(sum);

//     (*neuron).value = sum;
//     (*neuron).sig = out;
//     if (!isOutput)
//         (*neuron).delta = out * (1 - out) * sum;
// }

double mse(double expected, double output)
{
    return (expected - output) * (expected - output);
}

// double ErrorTotal(Layer *output, double *expected)
// {
//     double total = 0;   

//     for (int i = 0; i < output->neuronslen; i++)
//     {
//         output->neurons[i].delta = sigmoid_prime(output->neurons[i].sig) * (expected[i] - output->neurons[i].sig);

//         total += pow(output->neurons[i].delta, 2);
//     }
//     return total * 0.5;
//     // double err = 0;
//     // struct Layer *last = net->output;
//     // for(size_t i = 0; i < last->neuronslen; i++)
//     // {
        
//     //     last->neurons[i].delta = sigprime(last->neurons[i].sig) * (expected[i] - last->neurons[i].sig);
//     //     err += pow(last->neurons[i].delta, 2);
//     // }

//     // err *= 0.5;
//     // return err;
// }

void update_weights(Network *net,double lr)
{
    for(int i = 1; i < net->nbLayers; i++)
    {
        for(int j = 0; j < net->layers[i].neuronslen; j++)
        {
            for(int k = 0; k < net->layers[i-1].neuronslen; k++)
            {
                /*if(i == 0 && j==0 && k==0)
                {
                    printf("%f \n", net->layers[i].neurons[j].dw[k]);
                }*/
                // Update Weights
                net->layers[i].neurons[k].weights[j] -= (lr * net->layers[i].neurons[j].dw[k]);
                net->layers[i].neurons[k].dw[j] = 0;

            }
            // Update Bias
            net->layers[i].neurons[j].bias = net->layers[i].neurons[j].bias - (lr * net->layers[i].neurons[j].dbias);
            net->layers[i].neurons[j].dbias = 0;
        }
    }   
}

void backpropagation(Network *net, double *expected)
{
    for(size_t i = 0; i < LAST.neuronslen; i++)
    {           
        LAST.neurons[i].dz = (LAST.neurons[i].act - expected[i]) * sigmoid_prime(LAST.neurons[i].act);

        for(size_t k = 0; k < net->layers[net->nbLayers-2].neuronslen; k++)
        {   
            net->layers[net->nbLayers-2].neurons[i].dw[k] += (LAST.neurons[i].dz * net->layers[net->nbLayers-2].neurons[k].act);
            net->layers[net->nbLayers-2].neurons[k].dactv = net->layers[net->nbLayers-1].neurons[i].weights[k] * LAST.neurons[i].dz;
        }
            
        LAST.neurons[i].dbias += LAST.neurons[i].dz;
    }

    // Hidden Layers
    for(int i = net->nbLayers - 2; i > 0; i--)
    {
        for(int j = 0; j <net->layers[i].neuronslen; j++)
        {
            if(net->layers[i].neurons[j].zvalue >= 0 /*&& net->layers[i].neurons[j].z <= 1*/)
            {
                net->layers[i].neurons[j].dz = 1 * net->layers[i].neurons[j].dactv; 
            }  
            else
            {
                net->layers[i].neurons[j].dz = 0;
            }

            for(int k = 0; k <net->layers[i-1].neuronslen; k++)
            {
                net->layers[i-1].neurons[j].dw[k] = net->layers[i].neurons[j].dz * net->layers[i-1].neurons[k].act;    
                
                if(i>1)
                {
                    net->layers[i-1].neurons[k].dactv = net->layers[i].neurons[j].weights[k] * net->layers[i].neurons[j].dz;
                }
            }

            net->layers[i].neurons[j].dbias = net->layers[i].neurons[j].dz;
        }
    }
}


// void delta_hidden(Layer *layer)
// {
//     // int limit_next_layer = layer->NextLayer->neuronslen;

//     // for (int i = 0; i < layer->neuronslen; i++)
//     // {
//     //     double total = 0;   

//         // for (int i = 0; i < output->neuronslen; i++)
//         // {
//         //     output->neurons[i].delta = sigmoid_prime(output->neurons[i].sig) * (expected[i] - output->neurons[i].sig);
//         //     total += pow(output->neurons[i].delta, 2);
//         // }

// 	// }
//     //     layer->neurons[i].delta = total * 0.5;
// }


/*void delta_hidden(Layer *layer)
{
    int limit_next_layer = layer->NextLayer->neuronslen;

    for (int i = 0; i < layer->neuronslen; i++)
    {
        double delta_i = 0;
        
        for (int j = 0; j < limit_next_layer; j++)
        {
            double error_next = layer->NextLayer->neurons[j].delta;
            double d_out_next = layer->NextLayer->neurons[j].sig *
                (1 - layer->NextLayer->neurons[j].sig);
            double w_i = layer->NextLayer->neurons[j].weights[i];
            delta_i += error_next * d_out_next * w_i;
        
	}

        layer->neurons[i].delta = delta_i;
    }
}*/


// void update_weights_layer(Layer *layer, double lR)
// {
//     for (int i = 0; i < layer->neuronslen; i++)
//     {
//         //on each neuron we will update his weights thx to his delta
//         double delta_c = layer->neurons[i].delta;
//         layer->neurons[i].bias -= lR * delta_c;
//         for(int j = 0; j < layer->neurons[i].weightlen; j++)
//         {
//             double out = layer->PreviousLayer->neurons[j].sig;
//             layer->neurons[i].weights[j] -= lR * delta_c * out * (1 - out) * out;
//         }
//     }
// }

void freeNeuron(Neuron *n)
{
    free(n->weights);
}

void freeLayer(Layer *l)
{
    int len = l->neuronslen;

    for (int i = len - 1; i >= 0; i--)
        freeNeuron((*l).neurons + i);
}

void freeNetwork(Network *n)
{
    int len = n->nbLayers;

    for (int i = len - 1; i >= 0; i--)
        freeLayer((*n).layers + i);
}
