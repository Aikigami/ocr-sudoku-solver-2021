#include <stdio.h>
#include <stdlib.h>
#include <err.h>
// #include <gtk/gtk.h>
#include <math.h>

#include "../headers/bitmap.h"
#include "../headers/daux.h"
#include "../headers/r.h"
#include "../headers/segmentation.h"

int main_image()
{
	SDL_Surface *surface = load_image_surface("images/image_01.bmp");
	SDL_Surface *aled = Grayscale(surface);
	SDL_SaveBMP(aled, "gray.bmp");
	SDL_Surface *ah = toBlackAndWhite(aled);
	SDL_SaveBMP(ah, "bw.bmp");
	SDL_Surface *inverted = Invert(ah);
	int *h = Is_blank_line(inverted);
	//int *w = Is_blank_collum(inverted);
	//SDL_Surface *out = Rotation(ah);
	//SDL_SaveBMP(out, "out.bmp");
	int *max = malloc(1000 * sizeof(int));
	max = histo_max(h,1000);

	
	if(Init_Sdl())
	{
		errx(1,"Fail");
	}
	//gtk_init(&argc, &argv);
	//gtk_main();
	
	
	return 0;

}

