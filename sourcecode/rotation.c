#include <math.h>

#include "../headers/rotation.h"
#include "../headers/bitmap.h"

SDL_Surface *SDL_RotateImage(SDL_Surface *origine, float angle) {
    SDL_Surface *destination;
    int i;
    int j;
    Uint32 couleur;
    int mx, my, mxdest, mydest;
    int bx, by;
    float angle_radian;
    float tcos;
    float tsin;
    double largeurdest;
    double hauteurdest;

    angle_radian = -angle * M_PI / 180.0;

    tcos = cos(angle_radian);
    tsin = sin(angle_radian);

    largeurdest = ceil(origine->w * fabs(tcos) + origine->h * fabs(tsin)),
    hauteurdest = ceil(origine->w * fabs(tsin) + origine->h * fabs(tcos)),

    destination = SDL_CreateRGBSurface
            (0, largeurdest, hauteurdest, origine->format->BitsPerPixel,
             origine->format->Rmask, origine->format->Gmask, origine->format->Bmask,
             origine->format->Amask);

    for (j = 0; j < destination->h; j++) {
        for (i = 0; i < destination->w; i++) {
            put_pixel
            (destination, i, j,SDL_MapRGBA(destination->format, 255, 255, 255, 255));
        }
    }
    if (destination == NULL)
        return NULL;

    mxdest = destination->w / 2.;
    mydest = destination->h / 2.;
    mx = origine->w / 2.;
    my = origine->h / 2.;

    for (j = 0; j < destination->h; j++)
        for (i = 0; i < destination->w; i++) {
            bx = (ceil(tcos * (i - mxdest) + tsin * (j - mydest) + mx));
            by = (ceil(-tsin * (i - mxdest) + tcos * (j - mydest) + my));
            if (bx >= 0 && bx < origine->w && by >= 0 && by < origine->h) {
                couleur = get_pixel(origine, bx, by);
                put_pixel(destination, i, j, couleur);
            }
        }

    return destination;
}