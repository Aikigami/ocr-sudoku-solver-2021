#include "../headers/Train.h"
#include "../headers/Input.h"
#include <time.h>
#include "../headers/bitmap.h"
#include "../headers/Network.h"
void trainNetwork(Network *net, double lRate, int epoch)
{
    char path[100];
    double expected_result[9];
    srand(time(NULL));
    int count =1;
    for (int i = 0; i < 9; i++)
        expected_result[i] = 0;

    for(int i =0;i<epoch;i++)
    {
        int current =  (rand()%9)+1;
        int trainingnbr= (rand()%279);
 	    snprintf(path,sizeof(path),"../dataset/digit/%d/%d.bmp",current,trainingnbr);
        int *input = ImgToArray(load_image_surface(path));
	    int index = current-1;

        expected_result[index] = 1;
        startprop(net,input);
        feedForward(net);
        Generate_cost(net,index,expected_result,count);
        count++;
        //printf("expected: %d, result: %c\n", current, get_answer(net));
	    backpropagation(net, expected_result);
         if(i % 10 == 0)
            {
                update_weights(net,lRate);
            }
        expected_result[index] = 0;
        free(input);
    }
}
