#ifndef NEURON_H
#define NEURON_H

typedef struct Neuron
{
    int weightlen;
    
    double bias;
    double zvalue;
    double dactv;	
	double dbias;
	double dz;
    double act;

    double *weights;
    double *dw;
} Neuron;

#endif
