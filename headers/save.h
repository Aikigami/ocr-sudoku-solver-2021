#ifndef SAVE_H
#define SAVE_H
#include "Network.h"
void write_neuron(Neuron *neuron, int index, int index_max, FILE *fp);
void write_layer(Layer *layer, int index, int index_max, FILE *fp);

void write_network(Network *network, char *filename);

struct json_object *parse_network(char *filename);
void parse_neuron_from_file(struct json_object *neuron_object, Neuron *neuron);

void parse_layer_from_file(struct json_object *layer_object, Layer *layer);

void parse_network_from_file(char *filename, Network *network);

#endif