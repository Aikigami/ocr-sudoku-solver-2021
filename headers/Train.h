#ifndef TRAIN_H
#define TRAIN_H

#include "Network.h"

void trainNetwork(Network *n, double lrate, int epoch);

#endif