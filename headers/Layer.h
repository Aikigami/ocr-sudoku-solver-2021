#ifndef LAYER_H
#define LAYER_H

#include "Neuron.h"

typedef struct Layer
{
    Neuron *neurons;
    int neuronslen;
} Layer;

#endif