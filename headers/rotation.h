#ifndef ROTATION_H
#define ROTATION_H

#include "bitmap.h"

SDL_Surface *SDL_RotateImage(SDL_Surface *origine, float angle);

#endif
