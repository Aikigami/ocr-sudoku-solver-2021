#ifndef NETWORK_H
#define NETWORK_H

#include "Layer.h"
#include <stdlib.h>
#include <stdio.h>

typedef struct Network
{
    Layer *layers;
    int nbLayers;
    double fcost;
    double *cost;
} Network;

void SetNeuron(Neuron *neuron, int weightlen, double *weight, int bias);
void newNeuron(Neuron *neuron, int weightlen);
double sigmoid(double x);
double sigmoid_prime(double x);
double rdmDouble();
double ReLu(double x);
void startprop(Network *net,int *input);
void create_layer(Layer *layer, int size,int prevlen);
void feedForward(Network *net);
void propagation_layer(Layer *layer);
void create_network(Network *net,
                    int nbLayer,
                    int *nbNeurons);
char get_answer(Network *net);
void Generate_cost(Network *net,int current,double *expected,int count);
void backpropagation(Network *net, double *expected);
void sumNeuron(Neuron *neuron, Neuron *neededForA, int isOutput);
void update_weights(Network *net,double lr);
double mse(double expctd, double output);
double ErrorTotal(Layer *layer, double *expected);

void delta_hidden(Layer *layer);
void update_weights_layer(Layer *layer_working_on, double learning_rate);

void freeNetwork(Network *net);
void freeLayer(Layer *l);

#endif