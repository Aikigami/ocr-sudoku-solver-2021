#ifndef SEGMENTATION_H
#define SEGMENTATION_H

int *Is_blank_line(SDL_Surface *image);

int *Is_blank_collum(SDL_Surface *image);

int *histo_max(int *histo , size_t len);

int DetectLine(SDL_Surface *image, int x ,int y);

int Detectcollum(SDL_Surface *image, int x, int y);

void Detectiongrid(SDL_Surface *image);

int *Is_blank_line(SDL_Surface *image);

int *Is_blank_collum(SDL_Surface *image);

int *histo_max(int *histo , size_t len);

SDL_Surface *cut_image(SDL_Surface *image, int x , int y , int w , int h);

void Cutgrid(SDL_Surface *image, int x, int y, int w, int h);

#endif
